import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public logo: string;
  public logoName: string;
  public home: string;
  public wikiLink: string;
  public wikiLinkUrl: string;
  public about: string;

  constructor() {
    this.logo = '../../../assets/img/crystal-castles-white.png';
    this.logoName = 'Crystal Castles Logo';
    this.home = 'Home';
    this.wikiLink = 'Wiki';
    this.wikiLinkUrl = 'https://es.wikipedia.org/wiki/Crystal_Castles';
    this.about = 'About';

   }

  ngOnInit(): void {
  }

}
