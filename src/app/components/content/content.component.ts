import { CcAlbums } from './models/IcrystalCastlesAlbums';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {
  public albums: CcAlbums[]

  constructor() { 
    this.albums= [
      {
        albumName: 'Crystal Castles',
        albumCover: 'https://img.discogs.com/ooiFGPSkjVB63UpoJFkW615qWzg=/fit-in/600x598/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-1375511-1409999749-3563.jpeg.jpg',
        albumYear: 2008,
        albumDuration: '57 min',
        songNumber: 18,
        popularSongs: ['Alice Practice','Crimewave','Vanished','Magic Spells'], 
        singer: 'Alice Glass',
      },
      {
        albumName: '(II)',
        albumCover: 'https://images-na.ssl-images-amazon.com/images/I/71Nf9v20gzL._AC_SL1400_.jpg',
        albumYear: 2010,
        albumDuration: '52 min',
        songNumber: 14,
        popularSongs: ['Celestica', 'Baptism', 'Empathy', 'Not In Love'],
        singer: 'Alice Glass',
      },
      {
        albumName: '(III)' ,
        albumCover: 'https://upload.wikimedia.org/wikipedia/en/0/00/Crystal_Castles_-_III_album_cover.png',
        albumYear: 2012,
        albumDuration: '39 min',
        songNumber: 12,
        popularSongs: ['Plague', 'Kerosene', 'Affection', 'Telepath'],
        singer: 'Alice Glass',
      },
      {
        albumName: 'Amnesty (I)',
        albumCover: 'https://media.pitchfork.com/photos/5929bc6313d197565213b2e9/1:1/w_600/1881f03e.jpg',
        albumYear: 2016,
        albumDuration: '33 min',
        songNumber: 11,
        popularSongs: ['Femen', 'Char', 'Sadist', 'Chloroform'],
        singer: 'Edith Frances',
      },
      
    ]
  }

  ngOnInit(): void {
  }

}
