export interface CcAlbums {
    albumName: string;
    albumCover: string;
    albumYear: number;
    albumDuration: string;
    songNumber: number;
    popularSongs: string[];
    singer: 'Alice Glass' | 'Edith Frances',
}