import { ConvertActionBindingResult } from '@angular/compiler/src/compiler_util/expression_converter';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  public createdBy: string;
  public about: string;
  public contact: string;
  public year: number;


  constructor() {
    this.createdBy = "Antonio de la Llama";
    this.about = "About";
    this.contact = "Contact";
    this.year = 2020;
   }

  ngOnInit(): void {
  }

}
